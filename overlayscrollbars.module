<?php

/**
 * @file
 * General functions and hook implementations
 */

use \Drupal\Core\Routing\RouteMatchInterface;
use \Drupal\Core\Asset\AttachedAssetsInterface;

/**
 * Implements hook_help().
 */
function overlayscrollbars_help($route_name, RouteMatchInterface $route_match) {
  if ($route_name == 'help.page.overlayscrollbars') {
    $output = '<p>';
    $output .= t('What this module aims to accomplish is the ability to integrate OverlayScrollbars lib into a Drupal project and beautify browser default scrollbar.');
    $output .= '</p>';
    $output .= '<p>';
    $output .= t('See the <a href=":project_page">project page on Github</a> for more details.', [':project_page' => 'https://github.com/KingSora/OverlayScrollbars']);
    $output .= '</p>';

    return $output;
  }
}

/**
 * Implements hook_page_attachments_alter()
 */
function overlayscrollbars_page_attachments_alter(array &$page) {
  $page['#attached']['library'][] = 'overlayscrollbars/overlayscrollbars.min';
  $page['#attached']['library'][] = 'overlayscrollbars/overlayscrollbars';
}

/**
 * Implements hook_js_settings_alter()
 */
function overlayscrollbars_js_settings_alter(array &$settings, AttachedAssetsInterface $assets) {
  try {
    $config = \Drupal::config('overlayscrollbars.settings');
    $classList = $config->get('class_list') ?? FALSE;
    if ($classList && strlen($classList) > 0) {
      $classList = preg_split("/\r\n|\n|\r/", $classList);
      $classList = array_map('trim', $classList);
    }
    else {
      $classList = FALSE;
    }
    $elementsList = $config->get('elements_list') ?? FALSE;
    if ($elementsList && strlen($elementsList) > 0) {
      $elementsList = preg_split("/\r\n|\n|\r/", $elementsList);
      $elementsList = array_map('trim', $elementsList);
    }
    else {
      $elementsList = FALSE;
    }

    if (!isset($settings['overlayscrollbars'])) {
      $settings['overlayscrollbars'] = [];
    }
    $settings['overlayscrollbars']['classList'] = $classList;
    $settings['overlayscrollbars']['elementsList'] = $elementsList;

  } catch (\Throwable $th) {
    \Drupal::logger('overlayscrollbars')->error('An error occurred: @message', ['@message' => $th->getMessage()]);
  }

}
