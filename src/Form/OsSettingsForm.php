<?php

namespace Drupal\overlayscrollbars\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Settings form for OverlayScrollbars.
 */
class OsSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'overlayscrollbars_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('overlayscrollbars.settings');
    $form = parent::buildForm($form, $form_state);

    $form['targets'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Targets'),
      '#description' => $this->t('Name the targets that you want to have overlayscrollbars. If none entered, "body" element and any element with class "overlay-scrollbar" will allways have applied these nice scrollbar.'),
      '#collapsible' => FALSE,
    ];
    $form['targets']['class_list'] = [
      '#type' => 'textarea',
      '#wysiwyg' => FALSE,
      '#allowed_formats' => [],
      '#title' => $this->t('Class or classes (optional)'),
      '#description' => $this->t("Provide a class or more that will be addressed to have OverlayScrollbar. Then you can use these classes throught out all of project's html. Insert one valid jQuery class by line (without '.'). E.g: overlay-scrollbar"),
      '#default_value' => $config->get('class_list') ?? 'overlay-scrollbar',
    ];

    $form['targets']['elements_list'] = [
      '#type' => 'textarea',
      '#wysiwyg' => FALSE,
      '#allowed_formats' => [],
      '#title' => $this->t('Element or elements (optional)'),
      '#description' => $this->t("Provide an element or more that will be addressed to have OverlayScrollbar. These elements should exist on your project's html. Insert one valid jQuery element by line. E.g: body, #some-id, .sidebar"),
      '#default_value' => $config->get('elements_list') ?? 'body',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $values = $form_state->getValues();
    $this->config('overlayscrollbars.settings')
      ->set('class_list', strlen(trim($values['class_list'])) > 0 ? trim($values['class_list']) : NULL)
      ->set('elements_list', strlen(trim($values['elements_list'])) > 0 ? trim($values['elements_list']) : NULL)
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['overlayscrollbars.settings'];
  }

}
