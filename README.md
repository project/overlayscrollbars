## INTRODUCTION

The overlayscrollbars module integrates OverlayScrollbars library into Drupal and beautify browser's scrollbar.


## REQUIREMENTS

Drupal core ^9.0

## INSTALLATION

Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/node/895232 for further information.

## CONFIGURATION
- Go to /admin/config/user-interface/overlayscrollbars
- Set any class or classes you want OverlayScrollbars to be applied to
- Set any specific element or elements you want OverlayScrollbars to be applied to
- These settings are optional. If none entered, "body" element and "overlay-scrollbar" will be considered by default

## MAINTAINERS

Current maintainers for Drupal 10:

- Filipe Vilas Boas (joao.vilasboas) - https://www.drupal.org/u/joaovilasboas
