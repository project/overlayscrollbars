((Drupal, once) => {

  const defaultClassList = ['overlay-scrollbar'];
  const defaultElementsList = ['body'];

  const overlayScrollbarsConfig = {
    overflow: {
      x: 'hidden',
    },
    scrollbars: {
      theme: 'os-theme-dark',
      visibility: 'auto',
      autoHide: 'leave',
      autoHideDelay: 1000,
      dragScroll: true,
      clickScroll: false,
      pointers: ['mouse', 'touch', 'pen'],
    },
  };

  // Control over the instances
  const instances = [];

  Drupal.behaviors.overlayscrollbars = {
    attach: function (context, settings) {

      // Getting OverlayScrollbars methods and classes
      const {
        OverlayScrollbars,
        ScrollbarsHidingPlugin,
        SizeObserverPlugin,
        ClickScrollPlugin
      } = OverlayScrollbarsGlobal;

      const classList = settings.overlayscrollbars.classList ? settings.overlayscrollbars.classList : defaultClassList;
      const elementsList = settings.overlayscrollbars.elementsList ? settings.overlayscrollbars.elementsList : defaultElementsList;

      // Applying by class to all possible elements
      if (classList) {
        classList.forEach(classItem => {
          once('overlayscrollbars-applied', `.${classItem}`, context).forEach(function (item) {
            const osInstance = OverlayScrollbars(item, overlayScrollbarsConfig);
            instances.push(osInstance);
          });
        });
      }
      // Applying individually
      if (elementsList) {
        elementsList.forEach(elemItem => {
          once('overlayscrollbars-applied', `${elemItem}`, context).forEach(function (item) {
            const osInstance = OverlayScrollbars(item, overlayScrollbarsConfig);
            instances.push(osInstance);
          });
        });
      }

    }
  };

})(Drupal, once);
